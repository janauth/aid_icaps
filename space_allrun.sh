#!/bin/bash

# GETTING VARIABLES
source config_mini.sh
# source config_texus.sh


echo "Der Projektname lautet: " $PROJECTNAME " und die Bilder liegen in: " $SOURCE_IMAGEPATH

# PROPOSAL REGIONS----------------------------------------------------------
echo "Begin with PROPOSALREGIONS" &&
python ./proposalregions/run_groh.py $PROJECTNAME ./$SOURCE_IMAGEPATH &&
echo "Successfully cleaned the images" &&
python $SCRIPT \
        --source ./cache/$PROJECTNAME/images \
        --weights $INFERENCE_WEIGHTS \
        --nosave \
        --project ./cache/$PROJECTNAME \
        --img 1024 \
        --conf 0.08 \
        --save-txt &&
echo "END of PROPOSALREGIONS" &&

# FEATURECALCULATION--------------------------------------------------------
#echo "BEGIN with FEATURECALCULATION" &&
python featurecalculation/run_featurecalculation.py $PROJECTNAME &&
echo "END of FEATURECALCULATION" &&

# check if output exist
if [ -d "output" ];
then
  echo "output does exist."
else
  mkdir output
fi

# check if PROJECTNAME does exist
if [ -d "output/$PROJECTNAME" ];
then echo "$PROJECTNAME does exist."
else
  mkdir output/$PROJECTNAME
fi

python ./send2earth/run_send2earth.py $PROJECTNAME &&

echo 'THE END'
