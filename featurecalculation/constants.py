import numpy as np

# INPUT
# path to the output of the previous subpoject. -> the proposal regions of the yolo network
path_propreg = r'./proposalregions/output/MINIMAL_EXAMPLES_inference/exp/labels/'
imagepath = r'./inputdata/MINIMAL_EXAMPLES/images/'

scaling = 1024  # The original image ist 1024x1024 but the yolo output ist normalized to 1

# FOR FEATURE CALCULATION
threshold = 0.6313  #160/255 wenn die images nicht da sind, wird darauf zurück gefallen
path2th_images = './cache/mini/images/'
th_images = ['Os7-S1 Camera000000.png',
             'Os7-S1 Camera000001.png',
             'Os7-S1 Camera000002.png',
             'Os7-S1 Camera000004.png',
             'Os7-S1 Camera000005.png']

# Offsett in the images
OFFSET = 10 # getting this from config files ...

# FOR ORDERING
p1 = np.array([4.,-3.])
p2 = np.array([10.,-7.0])

# OUTPUT
outputfilename = 'particlewise.feather'
