import constants as c
import pandas as pd
pd.set_option('display.max_columns', None)
import os
import glob
from PIL import Image
import numpy as np
import cv2
import matplotlib.pyplot as plt
import time
from os.path import exists
import matplotlib.image as mpimg
from scipy import ndimage
# from sklearn.preprocessing import MinMaxScaler

import random
random.seed(4)  # for repetition


def testfunction():
    print('This is a testfunction, that prints a path: ' + c.teststring)

def calculate_th():
    path = c.path2th_images
    imagenames = c.th_images


    list_of_mean = []

    for x in imagenames:
        if exists(path + x):
            img = mpimg.imread(path +x) #imports as array of float32 (0...1)
            mean = np.mean(img)
            list_of_mean.append(mean)
            
        else:
            print('File existiert nicht')
    
    if list_of_mean == []:
        th = c.threshold
    else:
        th = np.mean(list_of_mean)
    return th

def featurecalculation(path2images,path2labels):
    print('Begin width featurecalculation')
    # TODO Import Data and saving it to Dataframe
    # Inferenz files importieren und in dataframe schreiben
    
    stencil_gauss = (1/16)*np.array(([1,2,1],[2,4,2],[1,2,1]))
    stencil_laplace = np.array([[1,1,1],[1,-8,1],[1,1,1]])
    
    dl = []  # temporaere datalist erstellen
    path = path2labels + '*.txt'
    print(path)
    val = sorted(filter(os.path.isfile, glob.glob(path)))
    # val = random.sample(val, 10)  # Downsampling for development in number of images
    #dt = 0

    threshold = calculate_th()
    print('calculate th')
    print(threshold)

    for x in val:
        filename = os.path.basename(x).split('.')[0]
        imagefile = path2images  + filename + '.png'  # where is the file
        img = Image.open(imagefile)  # load image

        with open(x, "r") as filestream:
            for line in filestream:
                # GET THE DATA
                line = line.rstrip('\n')
                currentline = line.split(" ")
                klasse = currentline[0]  # maybe converting to bool

                # TODO ISSUE: convert to Integer, should be dividable width 2/even
                xpos = float(currentline[1]) * c.scaling
                ypos = float(currentline[2]) * c.scaling
                width = float(currentline[3]) * c.scaling
                height = float(currentline[4]) * c.scaling

                # CALCULATE THE BOX AROUND THE PARTICLE
                left = int(xpos - (width / 2))
                right = int(xpos + (width / 2))
                top = int(ypos - (height / 2))
                bottom = int(ypos + (height / 2))
                
                # Transform to add offset (to make the images a little bigger)
                left = left - c.OFFSET
                right = right + c.OFFSET
                top = top - c.OFFSET
                bottom = bottom + c.OFFSET
                
                # FIX out of bounds mistake
                if left < 0:
                    left = 0
                if top < 0:
                    top = 0
                if right > c.scaling:
                    right = c.scaling
                if bottom > c.scaling:
                    bottom = c.scaling

                # CROP THE PARTICLE
                cropped_image = img.crop((left, top, right, bottom))
                cropped_image = np.asarray(cropped_image)/255 # normalizing dtype=uint8 to 1

                # CALCULATE FEATURE -> sharpness                
                # 1.Step: aplying gaussian filter
                gauss = ndimage.convolve(cropped_image, stencil_gauss, mode='wrap')
                
                # laplace
                gausslaplace = ndimage.convolve(gauss,stencil_laplace, mode='wrap')
                laplace_variance = np.var(gausslaplace)

                # CALCULATE FEATURE -> size_pixelcount
                binary_image = np.where(cropped_image > threshold, 1, 0)
                # n_ones= np.count_nonzero((binary_image) == 1) # here is no particle in the pixel
                n_zeros = np.count_nonzero(binary_image == 0) # here is a particle in the pixel
                size_pixelcount = n_zeros
                
                # CALCULATE INTERESTINGNESS due to distance in linear space
                p = np.log(np.array([size_pixelcount,laplace_variance]))
                distance = np.cross(c.p2-c.p1,p-c.p1)/np.linalg.norm(c.p2-c.p1)
                interestingness = distance

                # COMBINE ALL INFORMATION ON THIS PARTICLE
                combined_line = [filename,
                                 klasse,
                                 left,
                                 right,
                                 top,
                                 bottom,
                                 size_pixelcount,
                                 laplace_variance,
                                 interestingness]

                # COMBINE ALL INFORMATION ON ALL PARTICLES IN A LIST
                dl.append(combined_line)

    # CONVERT IT TO PANDAS-DATAFRAME
    header = ['filename',
              'klasse',
              'left',
              'right',
              'top',
              'bottom',
              'size_pixelcount',
              'sharpness',
              'interestingness']

    data = pd.DataFrame(dl, columns=header)
    
    return data


def ordering(data):

    # ORDER DATAFRAME DUE TO INTERESTINGNESS in ascendeing order
    data.sort_values(by=['interestingness'], inplace=True, ascending=False)
    data.reset_index(drop=True, inplace=True)
    # print(data.tail())

    # CREATE INTEGER ORDER-VECTOR
    n_rows = len(data.filename)
    order = np.linspace(start=1,
                        stop=n_rows,
                        num=n_rows,
                        dtype=int)
    data['order'] = order  # add the integer-order-vector to the dataframe
    # print('Created and stored following table:')
    # print(data.describe())
    #plt.hist(data.sharpness,bins=100)
    # plt.show()
    return data


