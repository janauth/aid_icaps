# IMPORT helper funktions
import helper as h
# import constants as c
import argparse

parser = argparse.ArgumentParser(description='Calculate the features from the image and the proposal regions')
parser.add_argument('projectname', metavar='projectname', type=str, help='What is the name of the Project')
args = parser.parse_args()

projectname=args.projectname

#print(projectname)

path2images = './cache/' + projectname + '/images/'
path2inference = './cache/' + projectname + '/exp/labels/'

data = h.featurecalculation(path2images,path2inference)
data = h.ordering(data)

savepath = './cache/' + projectname + '/particlewise.feather'
data.to_feather(savepath)
