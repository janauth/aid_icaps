# import constant as c

from PIL import Image
from numpy import asarray
from matplotlib import pyplot as plt
from PIL import Image
import glob
import os


def deleteallheader(name,path):
    counter = int(0) # initialize counter for counting the processed images
    where = path + '/*.png'
    # print('Reading images from '+  where)
    for image_file in glob.iglob(where):

        # get filename
        filename = image_file.split("/")[-1]
       #  print('Bearbeite image: ' + filename)

        # load the image
        img = Image.open(image_file)

        # convert image to numpy array
        img = asarray(img)

        # crop the image
        img = img[32:]

        # display the image
        # plt.imshow(img, cmap='gray')
        # plt.show()

        # convert to image
        img = Image.fromarray(img)

        # create cache directory structure
        cachepath = './cache/' + name + '/images'
        os.makedirs(cachepath, exist_ok=True)

        # save as image
        path =  cachepath + '/' + filename
        #print(path)
        img.save(path)
        print(filename + ' ohne HEAD gespeichert')

        # update counter
        counter = counter + int(1)
    return counter

#def clear_groh():
#    # delete all images width deleted header
#    path = c.dest_imagepath + '*'
#    os.remove()

if __name__ == '__main__':
    deleteallheader()
