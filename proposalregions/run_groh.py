import helper as h # IMPORT helper functions
import argparse
# import constant as c


# Defining parser
parser = argparse.ArgumentParser(description='getting rid of header and save the images')

parser.add_argument('Project_name',metavar='Project_name',type=str,help='Enter a project name')
parser.add_argument('Origin_path',metavar='Origin_path',type=str,help="Wo liegen denn die Bilder?")

args = parser.parse_args()


Project_name = args.Project_name
Origin_path = args.Origin_path

counter = h.deleteallheader(Project_name,Origin_path)
print('Deleted header of '+ str(counter) + ' images.')
