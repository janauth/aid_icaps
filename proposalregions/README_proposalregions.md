# Documentation for proposalregions

## Inference
* [INPUT]: all png-files from the texus flight experiment (defined in constants.py)

* [OUTPUT]: txt-files. A single file for each image (output/xxxxyyyy.txt)

## Training
* [INPUT]: csv-files with mask information and corresponding images

* [OUTPUT]: trained yolo-model