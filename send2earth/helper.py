import constants as c
import os
import pandas as pd
from PIL import Image
import numpy as np


def passfeatherfile(projectname):
    # Windows
    # os.system('copy file1.txt file7.txt')

    # Unix
    unixcommand = 'cp ' + './cache/' + projectname + '/particlewise.feather '  + './output/' + projectname + '/.'
    os.system(unixcommand)
#    print('Copied ' + c.INPUT + ' to ' + c.OUTPUT_path)

def cropasend(projectname):
   path = r'./cache/' + projectname + '/particlewise.feather'

   outputdirectory = './output/' + projectname + '/cropped_images/'
   os.makedirs(outputdirectory, exist_ok=True)

   #Read feather file and loop over it
   db = pd.read_feather(path)

   # Iterate over each row ...
   for index, row in db.iterrows():
       filename = row["filename"]
       left = int(row['left'])-c.OFFSET
       right = int(row['right'])+c.OFFSET
       top = int(row['top'])-c.OFFSET
       bottom = int(row['bottom'])+c.OFFSET
       order = int(row['order'])

       # read image
       imagepath = r'./cache/' + projectname + '/images/' + filename + '.png'
       print(imagepath)
       img = Image.open(imagepath)  # load image

       # CROP THE PARTICLE
       cropped_image = img.crop((left, top, right, bottom))
       # cropped_image = np.asarray(cropped_image)

       # SAVE THE IMAGE
       # c_img = Image.fromarray(cropped_image)
       imagename = outputdirectory + str(order) + '_' + filename + '.png'
       cropped_image.save(imagename)

if __name__ == "__main__":
    os.chroot(os.path.dirname(os.getcwd()))  #changing directory is not permitted (ERROR 1)
    cropasend('mini')
