# IMPORT helper funktions
import helper as h
import argparse


parser = argparse.ArgumentParser(description='Sending cropped images and particlewise feather file to ground station')
parser.add_argument('projectname',metavar='projectname',type=str, help='Defining the Projectname')
args = parser.parse_args()

projectname=args.projectname

h.cropasend(projectname)
h.passfeatherfile(projectname)
