#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 16 18:49:03 2023

@author: auth
"""

import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import matplotlib
from matplotlib.offsetbox import OffsetImage, AnnotationBbox
import numpy as np
import pandas as pd


import constants as c


# Import Data
df = pd.read_feather(c.INPUTFILE)
# inf = df_inf[df_inf['class']=='0'] #Extract class=0

x = df['sharpness'].to_numpy()
y = df['size_pixelcount'].to_numpy()

print('Es sind ' + str(len(df)) + ' Partikel vorhanden.')

path = c.DATAPATH

arr = []
scale = 1
order_list = []
filename = []

for index, row in df.iterrows():
    fname = row['filename']
    order = row['order']
    top = row['top']
    bottom = row['bottom']
    left = row['left']
    right = row['right']

    helper1 = path + '/' + str(order) + '_' + fname + '.png'
    print(helper1)
    img = mpimg.imread(path + '/' + str(order) + '_'+ fname + '.png')
    img = 1 - img  # normalizing from uint8 to one
    arr.append(img)
    order_list.append(order)
    filename.append(fname)
arr = np.asarray(arr, dtype=object)
#print('arr shape/size/type: ' + str(arr[0].shape))

# Normalizing order to the range of 0...100
order_list_norm = np.asarray(order_list, dtype=float) #converting to float
order_list_norm = order_list_norm - order_list_norm.min()  # shifting with left end to zero
minmax = order_list_norm.max() - order_list_norm.min() # getting range
order_list_norm = order_list_norm / minmax # normalizing to 1
order_list_norm = order_list_norm * 100 # to 100


# colors = {'0': 'red','1':'blue'}
# create figure and plot scatter
fig = plt.figure()
ax = fig.add_subplot(111)

line = ax.scatter(x, y, 
                  marker="o", 
                  #facecolor=colors, 
                  alpha=1,
                  c = order_list,
                  cmap='YlGn',
                  #color='k', 
                  s=15)
plt.title("Featurspace")
plt.xlabel("sharpness [-]")
plt.ylabel("size [n_pixel]")
fig.colorbar(line)

# create the annotations box
im = OffsetImage(arr[0],
                 zoom=1, 
                 cmap=plt.cm.gray_r)

xybox = (50., 50.)  # Position der Box

ab = AnnotationBbox(im, (0, 0), xybox=xybox, xycoords='data',
                    boxcoords="offset points", pad=0.3, arrowprops=dict(arrowstyle="->"))
# add it to the axes and make it invisible
ax.add_artist(ab)
ab.set_visible(False)
#ax.legend()

def hover(event):
    # if the mouse is over the scatter points
    if line.contains(event)[0]:
        # find out the index within the array from the event
        ind, = line.contains(event)[1]["ind"]
        # get the figure size
        w, h = fig.get_size_inches() * fig.dpi
        ws = (event.x > w / 2.) * -1 + (event.x <= w / 2.)
        hs = (event.y > h / 2.) * -1 + (event.y <= h / 2.)
        # if event occurs in the top or right quadrant of the figure,
        # change the annotation box position relative to mouse.
        ab.xybox = (xybox[0] * ws, xybox[1] * hs)
        # make annotation box visible
        ab.set_visible(True)
        # place it at the position of the hovered scatter point
        ab.xy = (x[ind], y[ind])
        # set the image corresponding to that point
        im.set_data(arr[ind])
        # write down Frame and Position
        print('Frame: ' + filename[ind])
        print('order: ' + str(order_list[ind]))
    else:
        # if the mouse is not over a scatter point
        ab.set_visible(False)
    fig.canvas.draw_idle()

# add callback for mouse moves
fig.canvas.mpl_connect('motion_notify_event', hover)
plt.show()