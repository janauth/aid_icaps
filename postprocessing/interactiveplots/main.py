# This is a sample Python script.

# Press Umschalt+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import helper as h
import constants as c

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    h.interactiveplot_train(c.TRAINFILE)
    #h.interactiveplot_test(c.TESTFILE,SAMPLES=400)
    #h.singleimage(IMAGENAME='Os7-S1 Camera182382')
    #h.singleimageINF_train(IMAGENAME='Os7-S1 Camera182382')
    #h.singleimageINF_test(IMAGENAME='Os7-S1 Camera182382',SAMPLES=500)

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
