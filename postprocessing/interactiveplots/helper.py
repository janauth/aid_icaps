import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import matplotlib
from matplotlib.offsetbox import OffsetImage, AnnotationBbox
import numpy as np
import constants as c
import pandas as pd
import cv2
from matplotlib.patches import Rectangle
from PIL import Image
np.random.seed(42)

def interactiveplot_train(INPUTFILE):
    # Import Data
    df_inf = pd.read_feather(INPUTFILE)
    # inf = df_inf[df_inf['class']=='0'] #Extract class=0
    inf = df_inf

    x = inf['sharpness'].to_numpy()
    y = inf['size'].to_numpy()

    print('Es sind ' + str(len(df_inf)) + ' Partikel vorhanden.')

    path = c.DATAPATH

    arr = []
    labels = []
    colors = []
    scale = 1
    filename = []

    for index, row in inf.iterrows():
        fname = row['filename']
        # print(fname)
        label = row['class']
        xpos = row['xpos']
        ypos = row['ypos']
        width = scale * row['width']
        hight = scale * row['hight']
        left = int(xpos - (width / 2))
        right = int(xpos + (width / 2))
        top = int(ypos - (hight / 2))
        bottom = int(ypos + (hight / 2))
        # print([left,right,top,bottom])
        img = cv2.imread(path + '/' + fname + '.png')
        img = 1 - (img[:, :, 0] / 255)  # normalizing from uint8 to one
        cropped = img[top:bottom, left:right]
        arr.append(cropped)
        labels.append(label)
        filename.append(fname)
        if label == '0':
            color = '#91bfdb'
        else:
            color = '#fc8d59'
        colors.append(color)
    arr = np.asarray(arr, dtype=object)
    print('arr shape/size/type: ' + str(arr[0].shape))

    # colors = {'0': 'red','1':'blue'}
    # create figure and plot scatter
    fig = plt.figure()
    ax = fig.add_subplot(111)
    line = ax.scatter(x, y, marker="o", facecolor=colors, alpha=1, color='k', s=15)
    plt.title("Featurspace")
    plt.xlabel("sharpness [-]")
    plt.ylabel("size [-]")
    # create the annotations box
    im = OffsetImage(arr[0], zoom=1, cmap=plt.cm.gray_r)
    xybox = (50., 50.)  # Position der Box
    ab = AnnotationBbox(im, (0, 0), xybox=xybox, xycoords='data',
                        boxcoords="offset points", pad=0.3, arrowprops=dict(arrowstyle="->"))
    # add it to the axes and make it invisible
    ax.add_artist(ab)
    ab.set_visible(False)
    ax.legend()

    def hover(event):
        # if the mouse is over the scatter points
        if line.contains(event)[0]:
            # find out the index within the array from the event
            ind, = line.contains(event)[1]["ind"]
            # get the figure size
            w, h = fig.get_size_inches() * fig.dpi
            ws = (event.x > w / 2.) * -1 + (event.x <= w / 2.)
            hs = (event.y > h / 2.) * -1 + (event.y <= h / 2.)
            # if event occurs in the top or right quadrant of the figure,
            # change the annotation box position relative to mouse.
            ab.xybox = (xybox[0] * ws, xybox[1] * hs)
            # make annotation box visible
            ab.set_visible(True)
            # place it at the position of the hovered scatter point
            ab.xy = (x[ind], y[ind])
            # set the image corresponding to that point
            im.set_data(arr[ind])
            # write down Frame and Position
            print('Frame: ' + filename[ind])
        else:
            # if the mouse is not over a scatter point
            ab.set_visible(False)
        fig.canvas.draw_idle()

    # add callback for mouse moves
    fig.canvas.mpl_connect('motion_notify_event', hover)
    plt.show()

def interactiveplot_test(INPUTFILE,SAMPLES):
    # Import Data
    df_inf = pd.read_feather(INPUTFILE)
    # inf = df_inf[df_inf['class']=='0'] #Extract class=0
    inf = df_inf

    inf = inf.sample(n=SAMPLES, random_state=None)  # datensatz künstlich verkleinern auf n=xxx samples

    x = inf['sharpness'].to_numpy()
    y = inf['size'].to_numpy()

    print('Es sind ' + str(len(df_inf)) + ' Partikel vorhanden.')

    path = c.DATAPATH

    arr = []
    labels = []
    colors = []
    scale = 1
    filename = []

    for index, row in inf.iterrows():
        fname = row['filename']
        # print(fname)
        label = row['class']
        xpos = row['xpos']
        ypos = row['ypos']
        width = scale * row['width']
        hight = scale * row['hight']
        left = int(xpos - (width / 2))
        right = int(xpos + (width / 2))
        top = int(ypos - (hight / 2))
        bottom = int(ypos + (hight / 2))
        # print([left,right,top,bottom])
        img = cv2.imread(path + '/' + fname + '.png')
        img = 1 - (img[:, :, 0] / 255)  # normalizing from uint8 to one
        cropped = img[top:bottom, left:right]
        arr.append(cropped)
        labels.append(label)
        filename.append(fname)
        if label == '0':
            color = '#91bfdb'
        else:
            color = '#fc8d59'
        colors.append(color)
    arr = np.asarray(arr, dtype=object)
    print('arr shape/size/type: ' + str(arr[0].shape))

    # colors = {'0': 'red','1':'blue'}
    # create figure and plot scatter
    fig = plt.figure()
    ax = fig.add_subplot(111)
    line = ax.scatter(x, y, marker="o", facecolor=colors, alpha=1, color='k', s=15)
    plt.title("Featurspace")
    plt.xlabel("sharpness [-]")
    plt.ylabel("size [-]")
    # create the annotations box
    im = OffsetImage(arr[0], zoom=1, cmap=plt.cm.gray_r)
    xybox = (50., 50.)  # Position der Box
    ab = AnnotationBbox(im, (0, 0), xybox=xybox, xycoords='data',
                        boxcoords="offset points", pad=0.3, arrowprops=dict(arrowstyle="->"))
    # add it to the axes and make it invisible
    ax.add_artist(ab)
    ab.set_visible(False)
    ax.legend()

    def hover(event):
        # if the mouse is over the scatter points
        if line.contains(event)[0]:
            # find out the index within the array from the event
            ind, = line.contains(event)[1]["ind"]
            # get the figure size
            w, h = fig.get_size_inches() * fig.dpi
            ws = (event.x > w / 2.) * -1 + (event.x <= w / 2.)
            hs = (event.y > h / 2.) * -1 + (event.y <= h / 2.)
            # if event occurs in the top or right quadrant of the figure,
            # change the annotation box position relative to mouse.
            ab.xybox = (xybox[0] * ws, xybox[1] * hs)
            # make annotation box visible
            ab.set_visible(True)
            # place it at the position of the hovered scatter point
            ab.xy = (x[ind], y[ind])
            # set the image corresponding to that point
            im.set_data(arr[ind])
            # write down Frame and Position
            print('Frame: ' + filename[ind])
        else:
            # if the mouse is not over a scatter point
            ab.set_visible(False)
        fig.canvas.draw_idle()

    # add callback for mouse moves
    fig.canvas.mpl_connect('motion_notify_event', hover)
    plt.show()

def singleimage_train(IMAGENAME):
    #check if this image is in the training set
    #reading training feather
    df_inf = pd.read_feather(c.TRAINFILE)
    image_table = df_inf[df_inf['filename']== IMAGENAME]
    print(image_table)

    arr = []
    labels = []
    colors = []
    filename = []

    for index, row in image_table.iterrows():
        fname = row['filename']
        filename.append(fname)

        label = row['class']
        label.append(labels)

        xpos = row['xpos']
        ypos = row['ypos']
        width = scale * row['width']
        hight = scale * row['hight']
        left = int(xpos - (width / 2))
        right = int(xpos + (width / 2))
        top = int(ypos - (hight / 2))
        bottom = int(ypos + (hight / 2))

        ## print([left,right,top,bottom])
        #img = cv2.imread(path + '/' + fname + '.png')
        #img = 1 - (img[:, :, 0] / 255)  # normalizing from uint8 to one
        #cropped = img[top:bottom, left:right]
        #arr.append(cropped)
        #labels.append(label)

        if label == '0':
            color = '#91bfdb'
        else:
            color = '#fc8d59'
        colors.append(color)

    im = cv2.imread(c.DATAPATH +'/' + IMAGENAME + '.png')
    plt.imshow(im)
    plt.title(IMAGENAME)

    #draw rectangles arround the particles


    plt.show()

def singleimageINF_train(IMAGENAME):
    #check if this image is in the training set
    #reading training feather
    df_inf = pd.read_feather(c.TESTFILE)
    image_table = df_inf[df_inf['filename']== IMAGENAME]
    print(image_table)

    imagepath = c.DATAPATH + '/' + IMAGENAME + '.png'
    # display the image
    plt.imshow(Image.open(imagepath))

    image_arr = []
    image_labels = []
    image_colors = []
    image_filename = []
    image_marker = []
    image_size = []

    for index, row in image_table.iterrows():
        fname = row['filename']
        image_filename.append(fname)

        label = row['class']
        image_labels.append(label)

        image_marker.append('o')
        image_size.append(40)
        xpos = row['xpos']
        ypos = row['ypos']
        width = c.SCALE * row['width']
        hight = c.SCALE * row['hight']
        left = int(xpos - (width / 2))
        right = int(xpos + (width / 2))
        top = int(ypos - (hight / 2))
        bottom = int(ypos + (hight / 2))

        #print([left,right,top,bottom])
        img = cv2.imread(c.DATAPATH + '/' + IMAGENAME + '.png')
        img = 1 - (img[:, :, 0] / 255)  # normalizing from uint8 to one
        cropped = img[top:bottom, left:right]
        image_arr.append(cropped)
        image_labels.append(label)



        if label == '0':
            color = '#91bfdb'
        else:
            color = '#fc8d59'
        image_colors.append(color)

        #draw rectangles arround the particles
        # add rectangle
        plt.gca().add_patch(Rectangle((top , left), width, hight,
                                      edgecolor=color,
                                      facecolor='none',
                                      lw=2))

    plt.show()

    image_x = image_table['sharpness']
    image_y = image_table['size']
    #draw featurespace (training) and marker these particles
    # Import Data
    df_inf = pd.read_feather(c.TRAINFILE)
    # inf = df_inf[df_inf['class']=='0'] #Extract class=0
    inf = df_inf

    train_x = inf['sharpness']
    train_y = inf['size']
    #.to_numpy()
    print('Es sind ' + str(len(df_inf)) + ' Partikel vorhanden.')

    path = c.DATAPATH

    train_arr = []
    train_labels = []
    train_colors = []
    scale = 1
    train_filename = []
    train_size = []
    for index, row in inf.iterrows():
        fname = row['filename']
        # print(fname)
        label = row['class']
        xpos = row['xpos']
        ypos = row['ypos']
        width = scale * row['width']
        hight = scale * row['hight']
        left = int(xpos - (width / 2))
        right = int(xpos + (width / 2))
        top = int(ypos - (hight / 2))
        bottom = int(ypos + (hight / 2))
        # print([left,right,top,bottom])
        img = cv2.imread(c.DATAPATH + '/' + fname + '.png')
        img = 1 - (img[:, :, 0] / 255)  # normalizing from uint8 to one
        cropped = img[top:bottom, left:right]
        train_arr.append(cropped)
        train_labels.append(label)
        train_filename.append(fname)
        if label == '0':
            color = '#91bfdb'
        else:
            color = '#fc8d59'
        train_colors.append(color)
        train_size.append(15)
    #train_arr = np.asarray(train_arr, dtype=object)
    print('arr shape/size/type: ' + str(train_arr[0].shape))

    # colors = {'0': 'red','1':'blue'}

    #Todo: array appenden und aufrufbar machen
    x = pd.concat([image_x, train_x],ignore_index=True)
    x = x.to_numpy()

    y = pd.concat([image_y,train_y],ignore_index=True)
    y = y.to_numpy()

    arr = image_arr + train_arr
    arr = np.asarray(arr,dtype=object)

    filename = image_filename + train_filename
    filename = np.asarray(filename,dtype=object)

    labels = image_labels
    labels = np.asarray(labels,dtype=object)

    colors = image_colors + train_colors
    colors = np.asarray(colors,dtype=object)

    markers = image_marker
    markers = np.asarray(markers,dtype=object)

    size = image_size + train_size
    size = np.asarray(size)


    # create figure and plot scatter
    fig = plt.figure()
    ax = fig.add_subplot(111)
    line = ax.scatter(x, y, marker='o', facecolor=colors, alpha=1, color='k', s=size)
    plt.title("Featurspace")
    plt.xlabel("sharpness [-]")
    plt.ylabel("size [-]")
    # create the annotations box
    im = OffsetImage(arr[0], zoom=1, cmap=plt.cm.gray_r)
    xybox = (50., 50.)  # Position der Box
    ab = AnnotationBbox(im, (0, 0), xybox=xybox, xycoords='data',
                        boxcoords="offset points", pad=0.3, arrowprops=dict(arrowstyle="->"))
    # add it to the axes and make it invisible
    ax.add_artist(ab)
    ab.set_visible(False)
    #ax.legend()



    def hover(event):
        # if the mouse is over the scatter points
        if line.contains(event)[0]:
            # find out the index within the array from the event
            ind, = line.contains(event)[1]["ind"]
            # get the figure size
            w, h = fig.get_size_inches() * fig.dpi
            ws = (event.x > w / 2.) * -1 + (event.x <= w / 2.)
            hs = (event.y > h / 2.) * -1 + (event.y <= h / 2.)
            # if event occurs in the top or right quadrant of the figure,
            # change the annotation box position relative to mouse.
            ab.xybox = (xybox[0] * ws, xybox[1] * hs)
            # make annotation box visible
            ab.set_visible(True)
            # place it at the position of the hovered scatter point
            ab.xy = (x[ind], y[ind])
            # set the image corresponding to that point
            im.set_data(arr[ind])
            # write down Frame and Position
            print('Frame: ' + filename[ind])
        else:
            # if the mouse is not over a scatter point
            ab.set_visible(False)
        fig.canvas.draw_idle()

    # add callback for mouse moves
    fig.canvas.mpl_connect('motion_notify_event', hover)
    plt.show()

def singleimageINF_test(IMAGENAME,SAMPLES):
    #check if this image is in the training set
    #reading training feather
    df_inf = pd.read_feather(c.TESTFILE)
    image_table = df_inf[df_inf['filename']== IMAGENAME]
    print(image_table)

    imagepath = c.DATAPATH + '/' + IMAGENAME + '.png'
    # display the image
    plt.imshow(Image.open(imagepath))

    image_arr = []
    image_labels = []
    image_colors = []
    image_filename = []
    image_marker = []
    image_size = []

    for index, row in image_table.iterrows():
        fname = row['filename']
        image_filename.append(fname)

        label = row['class']
        image_labels.append(label)

        image_marker.append('o')
        image_size.append(40)
        xpos = row['xpos']
        ypos = row['ypos']
        width = c.SCALE * row['width']
        hight = c.SCALE * row['hight']
        left = int(xpos - (width / 2))
        right = int(xpos + (width / 2))
        top = int(ypos - (hight / 2))
        bottom = int(ypos + (hight / 2))

        #print([left,right,top,bottom])
        img = cv2.imread(c.DATAPATH + '/' + IMAGENAME + '.png')
        img = 1 - (img[:, :, 0] / 255)  # normalizing from uint8 to one
        cropped = img[top:bottom, left:right]
        image_arr.append(cropped)
        image_labels.append(label)



        if label == '0':
            color = '#91bfdb'
        else:
            color = '#fc8d59'
        image_colors.append(color)

        #draw rectangles arround the particles
        # add rectangle
        plt.gca().add_patch(Rectangle((top , left), width, hight,
                                      edgecolor=color,
                                      facecolor='none',
                                      lw=2))

    plt.show()

    image_x = image_table['sharpness']
    image_y = image_table['size']
    #draw featurespace (training) and marker these particles
    # Import Data
    df_inf = pd.read_feather(c.TESTFILE)
    # inf = df_inf[df_inf['class']=='0'] #Extract class=0
    inf = df_inf
    inf = inf.sample(n=SAMPLES, random_state=None)  # datensatz künstlich verkleinern auf n=xxx samples

    train_x = inf['sharpness']
    train_y = inf['size']
    #.to_numpy()
    print('Es sind ' + str(len(df_inf)) + ' Partikel vorhanden.')

    path = c.DATAPATH

    train_arr = []
    train_labels = []
    train_colors = []
    scale = 1
    train_filename = []
    train_size = []
    for index, row in inf.iterrows():
        fname = row['filename']
        # print(fname)
        label = row['class']
        xpos = row['xpos']
        ypos = row['ypos']
        width = scale * row['width']
        hight = scale * row['hight']
        left = int(xpos - (width / 2))
        right = int(xpos + (width / 2))
        top = int(ypos - (hight / 2))
        bottom = int(ypos + (hight / 2))
        # print([left,right,top,bottom])
        img = cv2.imread(c.DATAPATH + '/' + fname + '.png')
        img = 1 - (img[:, :, 0] / 255)  # normalizing from uint8 to one
        cropped = img[top:bottom, left:right]
        train_arr.append(cropped)
        train_labels.append(label)
        train_filename.append(fname)
        if label == '0':
            color = '#91bfdb'
        else:
            color = '#fc8d59'
        train_colors.append(color)
        train_size.append(15)
    #train_arr = np.asarray(train_arr, dtype=object)
    print('arr shape/size/type: ' + str(train_arr[0].shape))

    # colors = {'0': 'red','1':'blue'}

    #Todo: array appenden und aufrufbar machen
    x = pd.concat([image_x, train_x],ignore_index=True)
    x = x.to_numpy()

    y = pd.concat([image_y,train_y],ignore_index=True)
    y = y.to_numpy()

    arr = image_arr + train_arr
    arr = np.asarray(arr,dtype=object)

    filename = image_filename + train_filename
    filename = np.asarray(filename,dtype=object)

    labels = image_labels
    labels = np.asarray(labels,dtype=object)

    colors = image_colors + train_colors
    colors = np.asarray(colors,dtype=object)

    markers = image_marker
    markers = np.asarray(markers,dtype=object)

    size = image_size + train_size
    size = np.asarray(size)


    # create figure and plot scatter
    fig = plt.figure()
    ax = fig.add_subplot(111)
    line = ax.scatter(x, y, marker='o', facecolor=colors, alpha=1, color='k', s=size)
    plt.title("Featurspace")
    plt.xlabel("sharpness [-]")
    plt.ylabel("size [-]")
    # create the annotations box
    im = OffsetImage(arr[0], zoom=1, cmap=plt.cm.gray_r)
    xybox = (50., 50.)  # Position der Box
    ab = AnnotationBbox(im, (0, 0), xybox=xybox, xycoords='data',
                        boxcoords="offset points", pad=0.3, arrowprops=dict(arrowstyle="->"))
    # add it to the axes and make it invisible
    ax.add_artist(ab)
    ab.set_visible(False)
    #ax.legend()



    def hover(event):
        # if the mouse is over the scatter points
        if line.contains(event)[0]:
            # find out the index within the array from the event
            ind, = line.contains(event)[1]["ind"]
            # get the figure size
            w, h = fig.get_size_inches() * fig.dpi
            ws = (event.x > w / 2.) * -1 + (event.x <= w / 2.)
            hs = (event.y > h / 2.) * -1 + (event.y <= h / 2.)
            # if event occurs in the top or right quadrant of the figure,
            # change the annotation box position relative to mouse.
            ab.xybox = (xybox[0] * ws, xybox[1] * hs)
            # make annotation box visible
            ab.set_visible(True)
            # place it at the position of the hovered scatter point
            ab.xy = (x[ind], y[ind])
            # set the image corresponding to that point
            im.set_data(arr[ind])
            # write down Frame and Position
            print('Frame: ' + filename[ind])
        else:
            # if the mouse is not over a scatter point
            ab.set_visible(False)
        fig.canvas.draw_idle()

    # add callback for mouse moves
    fig.canvas.mpl_connect('motion_notify_event', hover)
    plt.show()