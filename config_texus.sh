#!/bin/bash


# test="Dies ist ein Test für Variablen"

# Gobal Variables
PROJECTNAME="texus"
SOURCE_IMAGEPATH="./inputdata/ICAPS/images"

# INFERENCE
SCRIPT="./proposalregions/yolov5/detect.py"
INFERENCE_WEIGHTS="./proposalregions/yolov5/runs/train/exp/weights/best.pt"

